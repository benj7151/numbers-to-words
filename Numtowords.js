const ones = ['','one','two','three','four','five','six','seven','eight','nine']
const teens = ['ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen']
const tens = ['', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety']

function numbersToWords() {
    for (let i = 1; i <= 1000; i++) {
        if (i < 10) {
            document.write(i + " " + ones[i] + " ")
        }
        else if (i > 9 && i < 20) {
            document.write(i + " " + teens[i - 10] + " ")
        }
        else if (i > 19 && i < 100) {
            let digits = String(i);
            let firstDigit = Number(digits[0]);
            let secondDigit = Number(digits[1]);
            document.write(i + " " + tens[firstDigit - 1] + " " + ones[secondDigit] + " ") 

        }
        else if (i > 99 && i < 1000){
            let digits = String(i)
            let firstDigit = Number(digits[0])
            let secondDigit = Number(digits[1])
            let thirdDigit = Number(digits[2])
            if (secondDigit == 0){
                
                document.write(i + " " + ones[firstDigit] + " hundred " + ones[thirdDigit] + " ")
            }
            else if (secondDigit == 1){
                
                document.write(i + " " + ones[firstDigit] + " hundred " + teens[thirdDigit] + " ")
            }
            else {
                document.write(i + " " + ones[firstDigit] + " hundred " + tens[secondDigit - 1] + " " + ones[thirdDigit] + " ")
            }
        }
        else {
            document.write(i + " " + "one thousand")
        }
    }
}
numbersToWords()